<div id="groupContainer">
    <h2><u>Welcome to <a href="/g/<?= $title ?>"><?= $title ?></a></u></h2>

    <div id="description">
        <?= $description_raw ?>
    </div>

    <?php
    $groupID = "g_" . $id;

    if ($loggedIn) {
        echo "<br><div style='font-size:1.2em;'>Write a post:</div>";
        $this->load->view('templates/submit', array('parentID' => $groupID));
    }
    ?>
    <br><a href='javascript:void(0)' onClick='collapseAll($(this))'>Collapse All</a><br>
    <div id="postsContainer">
    <?php
    if (!$posts)
        echo 'There are no posts here.';
    foreach ($posts as $comment) {
        $this->load->view('templates/post', ['comment' => $comment]);
    }
    ?>
    </div>
</div>