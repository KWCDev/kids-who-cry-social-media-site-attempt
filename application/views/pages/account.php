<link rel="stylesheet" type="text/css" href="/assets/css/account.css">
<?php
$fromPage = filter_input(INPUT_GET, 'from');

if ($loggedIn) {
    ?>
    <h2>Welcome to your account. There is not much to see here.</h2>
    <div class="error"></div>
    <form method="POST" accept-charset="utf-8" id="settings">
        <h3 style="text-align: left;">Settings</h3>

        <input type="hidden" name="from" value="<?= $fromPage ?>">
        Display Name: <input type="text" name="displayName" value="<?= $displayName ?>" placeholder="Not required."/><br />
        Recovery Email: <input type="email" name="email" value='<?= $email ?>' placeholder="In case you forget you password."><br />
        <hr>
        <h3 style="text-align: left;">Change Password</h3>

        Current Password<span class='asterix'></span>: <input type="password" name="currentPass"/><br />
        New Password<span class='asterix'></span>: <input type="password" name="password1"/><br />
        New Confirm<span class='asterix'></span>: <input type="password" name="password2" /><br />
        <button type="button" onClick='saveAccount($(this).parent())'>Save</button>
    </form>

<?php } else { // Not logged in:  ?>
    <div id='loginPanel'>
        <div class='formColumn'>
            <h4>CREATE A NEW ACCOUNT</h4>
            <div id='createError' class="error"></div>
            <form id="signUpForm" action="/account/createAccount" method="POST" accept-charset="utf-8">
                <input type="hidden" name="from" value="<?= $fromPage ?>">
                <table border="0" cellpadding="4">
                    <tbody>
                        <tr>
                            <td class='inputLabel'>Username<span class='asterix'></span>:</td>
                            <td><input type="text" name="username" id="newusername" placeholder="Must be unique"/></td>
                        </tr>
                        <tr>
                            <td class='inputLabel'>Display Name:</td>
                            <td><input type="text" name="displayName" id="displayName" placeholder="Not required Ex: First/Last"/></td>
                        </tr>
                        <tr>
                            <td class='inputLabel'>Recovery Email:</td>
                            <td><input type="email" name="email" id="email" placeholder="For password forget"/></td>
                        </tr>
                        <tr>
                            <td class='inputLabel'>Password<span class='asterix'></span>:</td>
                            <td><input type="password" name="password1" id="password1"/></td>
                        </tr>
                        <tr>
                            <td class='inputLabel'>Confirm<span class='asterix'></span>:</td>
                            <td><input type="password" name="password2" id="password2"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><div style="width: 100%; display: inline-block;"><div class="g-recaptcha" data-sitekey="6Lc8Af0SAAAAAFBr0EK3-NhYbR1js2o7fTtHOlvc" style="float: right; "></div>
                                    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                                </div></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button onClick='$(this).SignUpSubmit();'>Create</button></td>
                        </tr>

                </table>

            </form>
        </div>
        <div class='formColumn'>
            <h4>LOGIN</h4>
            <div id='loginError' class="error"></div>
            <form id="loginForm" action="/account/login" method="POST" accept-charset="utf-8">
                <input type="hidden" name="from" value="<?= $fromPage ?>">
                <table border="0" cellpadding="4">
                    <tbody>
                        <tr>
                            <td class='inputLabel'>Username:</td>
                            <td><input type="text" name="username" id="username"/></td>
                        </tr>
                        <tr>
                            <td class='inputLabel'>Password:</td>
                            <td><input type="password" name="password" id="password"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button onClick='$(this).LoginSubmit();' class='inputLabel'>Login</button></td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
<?php } //End of login forms    ?> 