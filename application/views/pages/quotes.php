<style>
    blockquote {
        margin: 0px;
        border-left: 0px;
        quotes: "\201C" "\201D";
    }
    blockquote:before {
        color: black;
        content: open-quote;
        font-size: 3em;
        line-height: 0.1em;
        margin-right: 0.05em;
        vertical-align: -0.4em;
    }
    blockquote p {
        display: inline;
    }

    #quotesDiv h1 {
        text-decoration: underline;
        font-size: 1em;
        padding-top: 1.5em;
    }
</style>

<img src='assets/ibsymbol.jpeg' alt="IB symbol" width='70' style=" float:left;"><h1 id="headingQuotes"> &nbsp;Famous Quotes and Moments of Alumni </h1>

<div id='quotesDiv'>

    <h1>December 5, 2012:</h1>

    <blockquote>Blaise Vance: "What is this?"</blockquote>
    <blockquote>Aitana Arguedas: "It's a support group for drug addicts"</blockquote>
    <blockquote>Julia Nassar: "Basically."</blockquote>

    <h1> February 25, 2013:</h1>
    <blockquote>Claudia Gardner: "Can someone please tell me what you guys learned in the workshop?"</blockquote>
    <blockquote>(Julia Nassar): "Absolutely nothing."</blockquote>
    <blockquote>(Julia Nassar): " No that's not true. I learned the IB students should not be mixed in with the regular kids." (+4)</blockquote>
    <blockquote>Kevin Campbell:  IB SEGREGATION IS THE ONLY KIND I SUPPORT (+3)</blockquote>

    <h1> April 25, 2013:</h1>
    <blockquote>Harold Eduardo Mantilla Capacho changed the name of the group "GStar IB Class of 2015" to "Kids Who Cry"</blockquote>

    <h1> April 25, 2013:</h1>
    <blockquote>Harold Eduardo Mantilla Capacho changed the name of the group "Kids Who Cry" to "GStar IB Class of 2015".</blockquote>
    <blockquote>Kevin Campbell: <br>&nbsp;&nbsp; "nononono" (+2)<br>&nbsp;&nbsp; "back" (+2)<br>&nbsp;&nbsp; "now" (+2)<br>&nbsp;&nbsp; "change it now" (+1)</blockquote>
    <blockquote>Harold Mantilla: "Oh God" <br>&nbsp;&nbsp; "I'm sorry" <br>&nbsp;&nbsp; "what"</blockquote>
    <blockquote>Shaun Besman: "CHANGE IT BACK YOU MONKEY!" (+2)</blockquote>
    <blockquote>Kevin Campbell: "thank you" <br>&nbsp;&nbsp; "rusty ass" (+3)</blockquote>

    <h1> April 25, 2013:</h1>
    <em>I am Harold, I was lazy and did not put any of these in. (10.4.14)</em>
    <h1>I am harold</h1><p>Yes you are. -Keely</p>
</div>

