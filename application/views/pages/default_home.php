<div style="text-align: center;">
    <h1 id="mainHeading" style="font-size: 3em;">Kids Who Cry</h1>
    <h2 id="headingCaption" style="margin-top: 0;"><u>For students, by students.</u></h2> 
    <div style="overflow: auto; padding: auto 0;">
        <img id="mainImage" src='assets/kids.jpg' alt="Kids">
        <h3 style="display: inline-block; vertical-align: middle; max-width: 48%;">
            Welcome to Kids Who Cry! 
            <br><br> 
            A web site designed for the cooperation between present candidates and the guidance of past 
            candidates where the content is controlled by <i>YOU</i> ...textbooks are mandatory, clothes are optional.</h3>
    </div>
</div>

<h2> Directory: </h2>
<a href="quotes">Famous Quotes and Moments of Alumni (Illuminati)</a>
<p> Enjoy your stay! </p>
<br><br><br>
<h2> Album: </h2>
<br>
<img id="mainImage" src='assets/shaun.jpg' alt="Shaun">
<img id="mainImage" src='assets/kevin.jpg' alt="Kids">

<h2>Parsedown Testing</h2> 
<?php
$Parsedown = new Parsedown();
echo $Parsedown->text('Look: <collaspe>In the under.</collaspe>');
?>

<!-- The following are some Material Design Color Palette. -->

<h2>COLORS!</h2>

<ul>
    <li style="background-color: #00bcd4;">
        <span>Cyan</span>
        <span>500 </span>
        <span>#00bcd4</span>
    </li>
    <li style="background-color: #e0f7fa;"><span>50 </span><span>#e0f7fa</span></li>
    <li style="background-color: #b2ebf2;"><span>100 </span><span>#b2ebf2</span></li>
    <li style="background-color: #80deea;"><span>200 </span><span>#80deea</span></li>
    <li style="background-color: #4dd0e1;"><span>300 </span><span>#4dd0e1</span></li>
    <li style="background-color: #26c6da;"><span>400 </span><span>#26c6da</span></li>
    <li style="background-color: #00bcd4;"><span>500 </span><span>#00bcd4</span></li>
    <li style="background-color: #00acc1;"><span>600 </span><span>#00acc1</span></li>
    <li style="background-color: #0097a7;"><span>700 </span><span>#0097a7</span></li>
    <li style="background-color: #00838f;"><span>800 </span><span>#00838f</span></li>
    <li style="background-color: #006064;"><span>900 </span><span>#006064</span></li>
    <li style="background-color: #84ffff;"><span>A100 </span><span>#84ffff</span></li>
    <li style="background-color: #18ffff;"><span>A200 </span><span>#18ffff</span></li>
    <li style="background-color: #00e5ff;"><span>A400 </span><span>#00e5ff</span></li>
    <li style="background-color: #00b8d4;"><span>A700 </span><span>#00b8d4</span></li>
</ul>

<ul>
    <li style="background-color: #ff5722;">
        <span>Deep Orange</span>
        <span>500 </span>
        <span >#ff5722</span>
    </li>
    <li style="background-color: #fbe9e7;"><span>50 </span><span>#fbe9e7</span></li>
    <li style="background-color: #ffccbc;"><span>100 </span><span>#ffccbc</span></li>
    <li style="background-color: #ffab91;"><span>200 </span><span>#ffab91</span></li>
    <li style="background-color: #ff8a65;"><span>300 </span><span>#ff8a65</span></li>
    <li style="background-color: #ff7043;"><span>400 </span><span>#ff7043</span></li>
    <li style="background-color: #ff5722;"><span>500 </span><span>#ff5722</span></li>
    <li style="background-color: #f4511e;"><span>600 </span><span>#f4511e</span></li>
    <li style="background-color: #e64a19;"><span>700 </span><span>#e64a19</span></li>
    <li style="background-color: #d84315;"><span>800 </span><span>#d84315</span></li>
    <li style="background-color: #bf360c;"><span>900 </span><span>#bf360c</span></li>
    <li style="background-color: #ff9e80;"><span>A100 </span><span>#ff9e80 </span></li>
    <li style="background-color: #ff6e40;"><span>A200 </span><span>#ff6e40 </span></li>
    <li style="background-color: #ff3d00;"><span>A400 </span><span>#ff3d00 </span></li>
    <li style="background-color: #dd2c00;"><span>A700 </span><span>#dd2c00 </span></li>
</ul>

<ul>
    <li style="background-color: #2196F3;">
        <span>Blue</span>
        <span>500 </span>
        <span>#2196f3</span>
    </li>
    <li style="background-color: #e3f2fd;"><span>50 </span><span>#e3f2fd</span></li>
    <li style="background-color: #bbdefb;"><span>100 </span><span>#bbdefb</span></li>
    <li style="background-color: #90caf9;"><span>200 </span><span>#90caf9</span></li>
    <li style="background-color: #64b5f6;"><span>300 </span><span>#64b5f6</span></li>
    <li style="background-color: #42a5f5;"><span>400 </span><span>#42a5f5</span></li>
    <li style="background-color: #2196f3;"><span>500 </span><span>#2196f3</span></li>
    <li style="background-color: #1e88e5;"><span>600 </span><span>#1e88e5</span></li>
    <li style="background-color: #1976d2;"><span>700 </span><span>#1976d2</span></li>
    <li style="background-color: #1565c0;"><span>800 </span><span>#1565c0 </span></li>
    <li style="background-color: #0d47a1;"><span>900 </span><span>#0d47a1</span></li>
    <li style="background-color: #82b1ff;"><span>A100 </span><span>#82b1ff</span></li>
    <li style="background-color: #448aff;"><span>A200 </span><span>#448aff</span></li>
    <li style="background-color: #2979ff;"><span>A400 </span><span>#2979ff</span></li>
    <li style="background-color: #2962ff;"><span>A700 </span><span>#2962ff</span></li>
</ul>

<ul>
    <li style="background-color: #3f51b5;">
        <span>Indigo</span>
        <span >500 </span>
        <span>#3f51b5</span>
    </li>
    <li style="background-color: #e8eaf6;"><span>50 </span><span>#e8eaf6</span></li>
    <li style="background-color: #c5cae9;"><span>100 </span><span>#c5cae9</span></li>
    <li style="background-color: #9fa8da;"><span>200 </span><span>#9fa8da</span></li>
    <li style="background-color: #7986cb;"><span>300 </span><span>#7986cb</span></li>
    <li style="background-color: #5c6bc0;"><span>400 </span><span>#5c6bc0 </span></li>
    <li style="background-color: #3f51b5;"><span>500 </span><span>#3f51b5</span></li>
    <li style="background-color: #3949ab;"><span>600 </span><span>#3949ab</span></li>
    <li style="background-color: #303f9f;"><span>700 </span><span>#303f9f</span></li>
    <li style="background-color: #283593;"><span>800 </span><span>#283593</span></li>
    <li style="background-color: #1a237e;"><span>900 </span><span>#1a237e</span></li>
    <li style="background-color: #8c9eff;"><span>A100 </span><span>#8c9eff</span></li>
    <li style="background-color: #536dfe;"><span>A200 </span><span>#536dfe</span></li>
    <li style="background-color: #3d5afe;"><span>A400 </span><span>#3d5afe</span></li>
    <li style="background-color: #304ffe;"><span>A700 </span><span>#304ffe</span></li>
</ul>