<style>
    #motd{
        display: none;
    }
    
    .hint:before {
        content: "  ";
    }
    .hint {
        font-size: 0.7em;
        color: #999;
    }
</style>
<div>
    <form action='createPOST' method='POST' accept-charset='utf-8'>
        <p>URL slug<i class="hint">no spaces</i></p>
        <input type="text" name="name" id="name" maxlength="50">
        <p>Title</p>
        <input type="text" name="title" id="title" maxlength="50">
        <p>Description<i class="hint">1024 characters max</i></p>
        <textarea name="description" placeholder="Who/What is it for?" maxlength="1024"></textarea>
        <p>Privacy?</p>
        <input type="radio" name="privacy" value="public">Public<br>
        <input type="radio" name="privacy" value="private">Private
        <div id='error' class='error'></div>
        <button type='button' onclick='createGroupPOST($(this).parent())'>Create</button>
    </form>
</div>




<?php $this->load->view('templates/formattingHelp'); ?>
