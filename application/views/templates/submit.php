<div class='submitPostDiv'>
    <form method='POST' accept-charset='utf-8'>
        <input type='hidden' name='parentID' value='<?= $parentID ?>'>
        <textarea name='text' placeholder='What would you like to say?' onclick="if ($(this).height() < 130) $(this).css('height', '9em');"></textarea>
        <div id='error' class='error'></div>
        <button type='button' onclick='Submit($(this).parent())'>Post</button>
        <button type='button' onclick="$(this).parent().find('textarea').css('height', '2em');">Cancel</button>
        <?php $this->load->view('templates/formattingHelp'); ?>
    </form>
</div>