<!DOCTYPE html>
<html lang="en">
    <?php
    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    function getMOTD() {
        $f_contents = file("assets/motd.txt");
        if (trim($f_contents[0]) == "*")
            $motd = $f_contents[1];
        else
            $motd = trim($f_contents[array_rand($f_contents)]);

        return str_replace('-', '<br>&nbsp;&nbsp;&ndash;', $motd);
    }

    $sessionStatusLink = $loggedIn ?
            "<a title='Sign out' href='/account/logout' class='fa fa-sign-out'></a>" :
            "<a title='Sign in' href='/account?from=/" . uri_string() . "' class='fa fa-sign-in'></a>";
    ?>
    <head>
        <title>Kids Who Cry Alpha</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">

        <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
        <link rel="stylesheet" href="/assets/css/font-awesome/css/font-awesome.min.css">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
        <!--<link href='http://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css' Causes Cross-Origin Ref Error >-->

        <!-- Mobile Things (may make this based on subdomain or uri rather than width.)-->
        <script type="text/javascript">
//            var loggedIn = <?php echo json_encode($loggedIn); ?>; 
//            if ($(window).width() < 600) {
//                document.write("<link rel='stylesheet' href='http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css' />");
//                document.write("<script type = 'text/javascript' src='http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js'>\x3C/script>");
//            }
        </script>
    </head>
    <body>
        <div id="header">
            <ul class="header">
                <li id="mainName"><a href='/'><b>Kids Who Cry</b></a></li>
            </ul>
            <ul class="header" style="right: 0;">
                <li><a href="/g/test">Test Group</a></li>
                <li><a href="/create">+</a></li>
                <li><a class='fa fa-search' href="/#Find a Group:Work in progress"></a></li>
                <li><?= $sessionStatusLink ?></li>
            </ul>
        </div>
        <div id="container">
            <div id="motd"><?php echo getMOTD(); ?></div>