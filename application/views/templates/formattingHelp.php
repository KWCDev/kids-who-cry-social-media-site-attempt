<script>
    function toggleFormattingHelp() {
        var helpTable = $('#MarkdownHelp');
        var helpLink = $('#formatHelpButton');
        helpTable.slideToggle(250, function () {
            helpTable.is(":visible") ? helpLink.text("Hide Help") : helpLink.text("Show Help");
        });
        var helpShowing = helpTable.is(':visible');
        helpTable.is(":visible") ? helpLink.text("Hide Help") : helpLink.text("Show Help");
    }
</script> 
<style>
    code {
        font-size: 1.2em;
    }
</style>
<a href="javascript:void(0)" id="formatHelpButton" onclick="toggleFormattingHelp();" style="float:right; font-size: .95em">Formatting Help</a>
<br>
<div style="display: none;" id="MarkdownHelp">
    <p>Use <a href="http://daringfireball.net/projects/markdown/syntax">Markdown syntax</a> for formatting posts. See bellow for the basics.</p>
    <table border="1" cellpadding="5" width="560px" style="border-collapse: collapse; border-radius: 50px;">
        <tr style="background-color: #ff9e80; text-align: center">
            <td><em>You type:</em></td>
            <td><em>You see:</em></td>
        </tr>
        <tr>
            <td><code>Hello word!</code></td>
            <td>Hello world!</td>
        </tr>
        <tr>
            <td><code>*italics*</code></td>
            <td><em>italics</em></td>
        </tr>
        <tr>
            <td><code>**bold**</code></td>
            <td><b>bold</b></td>
        </tr>
        <tr>
            <td><code>[Example!](http://example.com)</code></td>
            <td><a href="http://example.com">Example!</a></td>
        </tr>
        <tr>
            <td><code>![](abstract.png)</code></td>
            <td><img src="http://lorempixel.com/200/35/abstract/" alt="Example" /></td>
        </tr>
        <tr>
            <td>
                <code>
                    * item 1<br/>
                    * item 2<br/>
                    * item 3
                </code>
            </td>
            <td>
                <ul style="margin: 0 auto;">
                    <li>item 1</li>
                    <li>item 2</li>
                    <li>item 3</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><code>&gt;quoted text</code></td>
            <td><blockquote style="margin: 0">quoted text</blockquote></td>
        </tr>
        <tr>
            <td>
                <code>
                    Lines starting with four spaces <br/>
                    are treated like code:<br/><br/>
                    <span class="spaces">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    if 1 * 2 &lt; 3:<br/>
                    <span class="spaces">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    print "hello, world!"<br/>
                </code>
            </td>
            <td>Lines starting with four spaces <br/>
                are treated like code:<br/><br/>
                <pre>if 1 * 2 &lt; 3:<br/>&nbsp;&nbsp;&nbsp;&nbsp;print "hello, world!"</pre>
            </td>
        </tr>
        <tr>
            <td><code>~~strikethrough~~</code></td>
            <td><strike>strikethrough</strike></td>
        </tr>
    </table>
</div>