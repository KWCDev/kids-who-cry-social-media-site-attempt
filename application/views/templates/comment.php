<?php
$id = 'c_' . $id;

if (isset($groupname)) {
    $gLink = "/g/" . $groupname;
    $groupname = "&middot to <a href=$gLink title='$groupname page'>$gLink</a>";
} else
    $groupname = "";

$numCommentsString = $num_children . " comments";
if ($num_children == 1)
    $numCommentsString = $num_children . " comment";

if ($score == NULL)
    $score = "0";

switch ($user_vote) {
    case -1:
        $voteClass = "downed";
        break;
    case 0:
        $voteClass = "unvoted";
        break;
    case 1:
        $voteClass = "upped";
        break;
    default:
        $voteClass = "unvoted";
        break;
}
?>

<div class='comment' id='<?= $id; ?>'>
    <?php if ($this->session->userdata('logged_in')): ?>
        <div class="vote <?= $voteClass ?>" style="float: left; font-size: 1.3em;">
            <i onclick="upclick($(this))" class="uparrow fa fa-angle-up fa-lg"></i>
            <div class="score"><?= $score ?></div>
            <i onclick="downclick($(this))" class="downarrow fa fa-angle-down fa-lg"></i>
        </div>
    <?php endif; ?>

    <div style="overflow: hidden; resize: none;">
        <ul class="statuses">
            <li><a href="#"><?= $username ?></a></li>
            <li>&middot;</li>
            <li><?php echo humanTiming(strtotime($datecreated)) . " ago"; ?></li>
            <li>&middot;</li>
            <li onclick="$(this).parent().parent().siblings('.children').toggle('fast');"><?= $numCommentsString ?></li>
            <li><?= $groupname ?></li>
        </ul>

        <div class="commentText" style="display: block;"><?= $html ?></div>

        <?php if ($this->session->userdata('logged_in')): ?>
            <span class='action'><span>1</span><a onclick="replyTo('<?= $id ?>', $(this).parent())" class="fa fa-comment-o" href="javascript:void(0)"></a></span>
            <span class='action'><a onclick="alert('no work yet')" class="fa fa-trash-o" href="javascript:void(0)"></a></span>
        <?php endif; ?>
    </div>
    <div class="children">