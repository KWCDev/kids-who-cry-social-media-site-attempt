<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function shaHash($toHash) {
    return hash("sha256", $toHash);
}

class Usermodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function isUniqueUsername($username) {
        $query = $this->db->get_where('account', array('username' => strtolower($username)), 1, 0);
        return $query->num_rows() === 0 ? true : false;
    }

    public function isUniqueEmail($email) {
        $query = $this->db->get_where('account', array('email' => strtolower($email)), 1, 0);
        return $query->num_rows() === 0 ? true : false;
    }

    public function getColumn($columnName) {
        $id = $this->session->userdata('user_id');

        if ($id) {

            $this->db->select($columnName);
            $q = $this->db->get_where('account', ['id' => $id], 1, 0);
            return $q->result_array()[0][$columnName];
        } else {
            return;
        }
    }

    public function getDisplayName() {
        $displayName = $this->getColumn('display_name');
        return ($displayName === '') ? $this->getColumn('username') : $displayName;
    }

    protected $userID = -1;

    public function login($username, $password) {
        if ($this->confirmPassword($username, $password)) {
            $newdata = array(
                'user_id' => $this->userID,
                'username' => $username,
                'logged_in' => true,
            );
            $this->session->set_userdata($newdata);
            return true;
        }
        return false;
    }

    public function addUser() {
        $salt = $this->createSalt();
        $displayName = $this->input->post('displayName') ? 
                $this->input->post('displayName') : $this->input->post('username');
        $data = array(
            'username' => $this->input->post('username'),
            'display_name' => $displayName,
            'email' => strtolower($this->input->post('email')),
            'salt' => $salt,
            'passhash' => $this->hashPasswordSalt($this->input->post('password1'), $salt)
        );

        $this->db->insert('account', $data);
        $this->login($this->input->post('username'), $this->input->post('password1'));
    }

    public function resetPassword() {
        if ($this->confirmPassword($this->session->userdata('username'), $this->input->post('currentPass'))) {
            $salt = $this->createSalt();
            $passhash = $this->hashPasswordSalt($this->input->post('password1'), $salt);
            $newData = [
                'salt' => $salt,
                'passhash' => $passhash
            ];
            $this->db->where('id', $this->session->userdata('user_id'));
            $this->db->update('account', $newData);
            echo "Success.<script>window.location.href = '/account' </script>";
        } else {
            echo 'Incorrect current password.';
        }
    }

    public function updateSettings() {
        $newData = [
            'display_name' => $this->input->post('displayName'),
            'email' => $this->input->post('email')
        ];

        $this->db->where('id', $this->session->userdata('user_id'));
        $this->db->update('account', $newData);
        echo "Success.";
    }

    /* BTS Password Functions */

    public function confirmPassword($username, $password) {
        $query = $this->db->select('id, passhash, salt')->from('account')->like('LOWER(username)', strtolower($username))->get();

        if ($query->num_rows() === 1) {
            foreach ($query->result() as $rows) {
                $passHash = $this->hashPasswordSalt($password, $rows->salt);
                $this->userID = $rows->id;
                if ($passHash === $rows->passhash) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    function hashPasswordSalt($password, $salt) {
        return shaHash(shaHash($password) . shaHash($salt));
    }

    function createSalt() {
        $string = shaHash(uniqid(rand(), true));
        return substr($string, 0, 7);
    }

}
