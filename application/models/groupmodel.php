<?php

class groupModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->Parsedown = new Parsedown();
        $this->scoreQuery = "(SELECT SUM(vote) FROM vote WHERE comment_id = comments.id) AS score";

        $this->userID = $this->session->userdata('user_id');
        if (!$this->userID)
            $this->userID = -1;
        $this->userVoteQuery = "(SELECT vote FROM vote WHERE vote.user_id = " . $this->userID . " AND vote.comment_id = comments.id) AS user_vote";
    }

    public function createGroup() {
        $data = array(
            'name' => $this->input->post('name'),
            'title' => $this->input->post('title'),
            'description_raw' => $this->input->post('description'),
            'description_html' => $this->Parsedown->text($this->input->post('description'))
        );
        $this->db->insert('agroup', $data);
    }

    public function groupExists($groupName) {
        $q = $this->db->select('*')
                ->from('agroup')
                ->order_by('datecreated asc')
                ->like('LOWER(name)', strtolower($groupName))
                ->get();

        return $q->num_rows() === 0 ? false : true;
    }

    public function getGroupIDFromName($name) {
        return $this->db->select('id')->from('agroup')->where('name', $name)->get()->result_array()[0]['id'];
    }

    /**
     * @param string $groupName the groupname
     * @return array group id, title, and description_raw 
     */
    public function getGroupInfo($groupName) {
        $this->db->select('id, title, description_raw')->where('name', $groupName);
        return $this->db->get('agroup')->result_array()[0];
    }

    // TODO: Make this work.
    public function userSubscribedTo($group) {
        $userID = $this->session->userdata('user_id');
        if ($userID) {
            // Check to see if $groupID is in the users    
            return true;
        }
        return true; //for testing without login.
    }

    // TODO: Make this work.
    public function isPrivate($groupID) {
        if ($groupID) {
            return true;
        }
        return true; //for testing without login.
    }

    public function canAccess($group) {
        return $this->userSubscribedTo($group) || !$this->isPrivate($group);
    }

    public function setSubscription($subType, $groupID) {
        if (in_array($subType, [-1, 0, 1, 2])) {
//            $this->db->set('subscription', $subType)
//                    ->where('user_id', $this->userID) 
//                    ->where('group_id', $groupID);
        } else
            echo(json_encode(["error" => "Could not set subcription because of impropper `type` value."]));
    }

    public function insertPostForGroupID($gID, $userID, $rawText) {
        $htmlString = $this->Parsedown->text($rawText);

        $commentData = array(
            'submitterid' => $userID,
            'raw_text' => $rawText,
            'html' => $htmlString,
            'groupid' => $gID
        );

        $this->db->insert('comments', $commentData);
        $insertID = $this->db->insert_id();
        $this->db->insert('posts', ['groupid' => $gID, 'commentid' => $insertID]);

        return $this->getCommentForID($insertID, $gID);
    }

    public function insertCommentForParentID($pID, $gID, $userID, $rawText) {
        $htmlString = $this->Parsedown->text($rawText);

        $commentData = array(
            'submitterid' => $userID,
            'raw_text' => $rawText,
            'html' => $htmlString,
            'parent_id' => $pID,
            'groupid' => $gID
        );

        $this->db->insert('comments', $commentData);
        return $this->getCommentForID($this->db->insert_id(), $gID);
    }

    /**
     * Fetches the DB for an array of groups and their posts
     * @param array|int $groupIDs array of int's or a single int.
     * @return array with only one index with key of 'posts'
     */
    public function getPosts($groupIDs) {
        if ($groupIDs) {
            $q = $this->db->select("html, comments.datecreated, username, comments.id,
                            (SELECT count(*) FROM comments WHERE parent_id=posts.commentid) AS num_children,
                            (SELECT name FROM agroup WHERE id=posts.groupid) as groupname," .
                            $this->userVoteQuery . "," .
                            $this->scoreQuery)
                    ->join('comments', 'comments.id = posts.commentid')
                    ->join('account', 'account.id = comments.submitterid')
                    ->where_in('posts.groupid', $groupIDs)
                    ->order_by('datecreated', 'desc')
                    ->get('posts', 25, 0);

            return ['posts' => $q->result_array()];
        } else {
            return 'Something went wrong. Complain to the developer. Report: In Group Model. Group NULL.';
        }
    }

    /**
     * Fetches the database for the users subsciptions.
     * @param bigint $userID the id of the user usually from the session
     * @return array array with only one index with key of 'posts' 
     * or an empty array(if user has no subs)
     */
    public function getPostsForUser($userID) {
        $q = $this->db->select('subscriptions')->from('account')->where('id', $userID)->get();
        if ($q->result_array()[0]['subscriptions'] == NULL)
            return $this->getPosts(-1);
        $subscriptions = explode(',', str_replace(['{', '}'], "", $q->result_array()[0]['subscriptions']));
        return $this->getPosts($subscriptions);
    }

    private function getCommentForID($cID, $gID) {
        $q = $this->db->select("html, datecreated, comments.id, account.username,
                (SELECT count(*) FROM comments WHERE parent_id=" . $cID . ") AS num_children," .
                        $this->userVoteQuery . "," .
                        $this->scoreQuery)
                ->join('account', 'account.id = comments.submitterid')
                ->where('comments.id', $cID)
                ->where('comments.groupid', $gID)
                ->get('comments');
        if (!empty($q->result_array()[0]))
            return $q->result_array()[0];
        else
            die('No results.');
    }

    private function getCommentsForID($cID, $gID) {
        $comment = $this->getCommentForID($cID, $gID);
        $r = $this->db->select('comments.id')
                ->where('comments.parent_id', $cID)
                ->order_by('datecreated', 'desc')
                ->get('comments');
        $children = [];
        foreach ($r->result_array() as $child) {
            $children[] = $this->getCommentsForID($child['id'], $gID);
        }
        return array("comment" => $comment, "children" => $children);
    }

    public function getCommentTree($postID, $group) {
        $gID = $this->getGroupIDFromName($group);
        // $postID is the same as the top comment ID
        if ($this->canAccess($group))
            return $this->getCommentsForID($postID, $gID);
        else
            die("User can't access that group.");
    }

    public function setVoteForComment($userID, $voteNum, $commentID) {
        if ($voteNum == 0) {
            $this->db->delete('vote', ['comment_id' => $commentID]);
        } else if ($voteNum == -1 || $voteNum == 1) {
            // add/change
            $vote = [
                'user_id' => $userID,
                'vote' => $voteNum,
                'comment_id' => $commentID
            ];
            if ($this->db->select("comment_id")
                            ->where('comment_id', $commentID)
                            ->where('user_id', $userID)
                            ->get('vote')->num_rows() == 0) {
                $this->db->insert('vote', $vote);
            } else {
                $this->db->where('comment_id', $commentID)->where('user_id', $userID)->update('vote', $vote);
            }
        } else
            echo 'Improper vote.';
    }

}
