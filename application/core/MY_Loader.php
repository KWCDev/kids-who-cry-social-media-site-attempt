<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
    /* This is a custom method for rendering a page in full with the body as input. */
    public function page($template_name, $pageVars = array(), $return = FALSE) {
        $CI = & get_instance();
        $vars = array("loggedIn" => $CI->session->userdata('logged_in')) + $pageVars;
       
        $content = $this->view('templates/head', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('templates/foot', $vars, $return);

        if ($return) {
            return $content;
        }
    }

}
