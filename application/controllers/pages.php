<?php

class pages extends CI_Controller {

    public function view($page = '/default_home') {
        if ($page == 'emailtest') {
            $this->load->library('email');
                    
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $this->email->from('keely@keelyhill.com', 'Kid Who Cry');
            $this->email->to('keelyhill@gmx.com');
            $this->email->subject('Email Test to GMX');
            $this->email->message('<h2>Is this HTML?!!?</h2>');
            
            $this->email->send();

            echo $this->email->print_debugger();
            die();
        }

        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            show_404();
        }

        $data = [];
        /* Setup nessisary assets */
        $this->load->helper('form'); // I don't think this is needed, check later.
        // Check if logged in and home 
        if ($this->session->userdata('username') && $page === '/default_home') {
            $this->load->model('groupmodel');
            $page = 'userhome';
            $data = $this->groupmodel->getPostsForUser($this->session->userdata('user_id'));
        }
        if (!$this->session->userdata('username') && $page === 'userhome') {
            $page = 'default_home';
        }

        /* Render */
        $this->load->page('pages/' . $page, $data);
    }

    public function createGroup() { // I dont think this is needed
        $this->load->page('templates/createGroup');
    }

    public function createGroupPOST() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', '', 'required|min_length[3]|xss_clean|alpha_dash|callback_name_check');
        $this->form_validation->set_rules('title', '', 'xss_clean');
        $this->form_validation->set_rules('description', '', 'required|min_length[3]|xss_clean');

        $this->form_validation->set_message('is_unique', 'That group name already exists.');

        if ($this->form_validation->run()) {
            $this->load->model('groupmodel');
            $this->groupmodel->createGroup();
        }

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>'); // also possible echo form_error('field name', '<div class="error">', '</div>');

        echo validation_errors();
    }

    private function name_check($str) {
        $this->load->model('groupmodel');
        if (!$this->groupmodel->groupExists($str)) {
            $this->form_validation->set_message('_name_check', 'That group name already exists.');
            return false;
        } else return true;
    }
}
