<?php

class account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('userModel');
    }

    public function index() {
//Manage these later
        $page = "account";

        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            show_404();
        }

        $data = [
            'displayName' => $this->userModel->getDisplayName(),
            'email' => $this->userModel->getColumn('email')
        ];

        $this->load->page('pages/' . $page, $data);
    }

    public function createAccount() {
        $privatekey = "6Lc8Af0SAAAAAMCC_554B9imF95ydUpg6yNZ-EiR";

        $reCaptcha = new ReCaptcha($privatekey);

        $resp = $reCaptcha->verifyResponse(
                $this->input->server("REMOTE_ADDR"),  $this->input->post("g-recaptcha-response")
        );
        
        if (!$resp->success) {
            // What happens when the CAPTCHA was entered incorrectly
            die("The reCAPTCHA wasn't entered correctly.");
        } else {
            $this->form_validation->set_rules('username', 'Username', 'required|alpha_dash|min_length[3]|trim|callback__username_check');
            $this->form_validation->set_rules('displayName', 'Display Name', 'callback__alpha_dash_space|min_length[3]');
            $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|callback__uniqueEmailCheck');            // there is also a uniqe email clause
            $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[8]|matches[password2]');
            $this->form_validation->set_rules('password2', 'Password Confimation', 'trim|required');

            if ($this->form_validation->run()) {
                $this->userModel->addUser();
                $from = json_encode($this->input->post('from'));
                echo "Success!<script>window.location.href = $from </script>";
            }
            echo validation_errors();
        }
    }

    function login() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'required');
        $this->form_validation->set_rules('password', 'required');

        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $result = $this->userModel->login($username, $password);

            if ($result) {
                $from = json_encode($this->input->post('from'));
                echo "Success.<script>window.location.href = $from </script>";
            } else {
                echo "Username and/or passwords do not match.";
            }
        }
        echo validation_errors();
    }

    function logout() {
        echo 'Logging out...';
        $this->session->sess_destroy();
        $this->toMainAccount();
    }

    public function save() {
        $this->form_validation->set_rules('displayName', 'Display Name', 'callback__alpha_dash_space|min_length[3]');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|callback__uniqueEmailCheck');
        $this->form_validation->set_rules('currentPass', 'Current Password', 'trim|min_length[8]');
        $this->form_validation->set_rules('password1', 'Password', 'trim|min_length[8]|matches[password2]');
        $this->form_validation->set_rules('password2', 'Password Confimation', 'trim');

        if ($this->form_validation->run()) {
            if ($this->input->post('currentPass') != '') {
                if ($this->input->post('password1') === '') {
                    echo 'You must enter into all password feilds before changing it.';
                } else {
                    $this->userModel->resetPassword();
                }
            }
            $this->userModel->updateSettings();
        }

        echo validation_errors();
    }

    private function toMainAccount() { //this redirects page is accessed directly. I dont think this works with AJAX and everything.
        redirect('/account', 'refresh');
    }

    /* Validation Callbacks */

    function alpha_dash_space($str) {
        return (!preg_match("/^[a-z0-9 .\-]+$/i", $str)) ? FALSE : TRUE;
    }

    function _username_check($str) {
        $invalidUsernames = array('about', 'access', 'account', 'accounts', 'add', 'address', 'adm', 'admin', 'administration', 'adult', 'advertising', 'affiliate', 'affiliates', 'ajax', 'analytics', 'android', 'anon', 'anonymous', 'api', 'app', 'apps', 'archive', 'atom', 'auth', 'authentication', 'avatar', 'backup', 'banner', 'banners', 'bin', 'billing', 'blog', 'blogs', 'board', 'bot', 'bots', 'business', 'chat', 'cache', 'cadastro', 'calendar', 'campaign', 'careers', 'cgi', 'client', 'cliente', 'code', 'comercial', 'compare', 'config', 'connect', 'contact', 'contest', 'create', 'code', 'compras', 'css', 'dashboard', 'data', 'db', 'design', 'delete', 'demo', 'design', 'designer', 'dev', 'devel', 'dir', 'directory', 'doc', 'docs', 'domain', 'download', 'downloads', 'edit', 'editor', 'email', 'ecommerce', 'forum', 'forums', 'faq', 'favorite', 'feed', 'feedback', 'flog', 'follow', 'file', 'files', 'free', 'ftp', 'gadget', 'gadgets', 'games', 'guest', 'group', 'groups', 'help', 'home', 'homepage', 'host', 'hosting', 'hostname', 'html', 'http', 'httpd', 'https', 'hpg', 'info', 'information', 'image', 'img', 'images', 'imap', 'index', 'invite', 'intranet', 'indice', 'ipad', 'iphone', 'irc', 'java', 'javascript', 'job', 'jobs', 'js', 'knowledgebase', 'log', 'login', 'logs', 'logout', 'list', 'lists', 'mail', 'mail1', 'mail2', 'mail3', 'mail4', 'mail5', 'mailer', 'mailing', 'mx', 'manager', 'marketing', 'master', 'me', 'media', 'message', 'microblog', 'microblogs', 'mine', 'mp3', 'msg', 'msn', 'mysql', 'messenger', 'mob', 'mobile', 'movie', 'movies', 'music', 'musicas', 'my', 'name', 'named', 'net', 'network', 'new', 'news', 'newsletter', 'nick', 'nickname', 'notes', 'noticias', 'ns', 'ns1', 'ns2', 'ns3', 'ns4', 'old', 'online', 'operator', 'order', 'orders', 'page', 'pager', 'pages', 'panel', 'password', 'perl', 'pic', 'pics', 'photo', 'photos', 'photoalbum', 'php', 'plugin', 'plugins', 'pop', 'pop3', 'post', 'postmaster', 'postfix', 'posts', 'profile', 'project', 'projects', 'promo', 'pub', 'public', 'python', 'random', 'register', 'registration', 'root', 'ruby', 'rss', 'sale', 'sales', 'sample', 'samples', 'script', 'scripts', 'secure', 'send', 'service', 'shop', 'sql', 'signup', 'signin', 'search', 'security', 'settings', 'setting', 'setup', 'site', 'sites', 'sitemap', 'smtp', 'soporte', 'ssh', 'stage', 'staging', 'start', 'subscribe', 'subdomain', 'suporte', 'support', 'stat', 'static', 'stats', 'status', 'store', 'stores', 'system', 'tablet', 'tablets', 'tech', 'telnet', 'test', 'test1', 'test2', 'test3', 'teste', 'tests', 'theme', 'themes', 'tmp', 'todo', 'task', 'tasks', 'tools', 'tv', 'talk', 'update', 'upload', 'url', 'user', 'username', 'usuario', 'usage', 'vendas', 'video', 'videos', 'visitor', 'win', 'ww', 'www', 'www1', 'www2', 'www3', 'www4', 'www5', 'www6', 'www7', 'wwww', 'wws', 'wwws', 'web', 'webmail', 'website', 'websites', 'webmaster', 'workshop', 'xxx', 'xpg', 'you', 'yourname', 'yourusername', 'yoursite', 'yourdomain');
        if (in_array(strtolower($str), $invalidUsernames) || !$this->userModel->isUniqueUsername($str)) {
            $this->form_validation->set_message('_username_check', 'That username can not be used.');
            return false;
        } else {
            return true;
        }
    }

    function _uniqueEmailCheck($email) {
        if ($email) {
            $this->form_validation->set_message('_uniqueEmailCheck', 'That email is already used by an account.');
            return $this->userModel->isUniqueEmail($email);
        }
    }

}
