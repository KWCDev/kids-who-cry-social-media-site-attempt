<?php

class g extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('groupmodel');
        $this->loggedIn = $this->session->userdata('logged_in');
        
        $this->groupName = $this->uri->segment(2);
        $this->groupID = $this->groupmodel->getGroupIDFromName($this->groupName);
    }

    public function group($group, $sortmethod = 'nothing') {
        if (!$group || !$this->groupmodel->groupExists($group)) { //   || group does not exist (query db)
            echo "That group does not exist.";
            show_404();
        }

        $gID = $this->groupmodel->getGroupIDFromName($group);
        if (!$this->groupmodel->canAccess($group)) {
            die("This is a private group"); // Will render a seperate page.
        }
        // do session stuff here

        $groupInfo = $this->groupmodel->getGroupInfo($group);
        $groupPosts = $this->groupmodel->getPosts($groupInfo['id']);

        foreach ($groupPosts['posts'] as &$post) {
            unset($post['groupname']);
        }

        $this->load->page('group', $groupInfo + $groupPosts);
    }

    public function edit() {
        // Only mods or something should have acccess to this page.
    }

    function submit($group) {
        if (!$this->loggedIn)
            die('You are not logged in.');
        else if (!$this->groupmodel->canAccess($group))
            die('You can not access that group.');
        else if ($this->input->post('text') === "")
            die('You\'ve gotta say something mate.');
        else if (!$this->input->post('parentID'))
            dieToHome();

        $COMMENT_MAX_LENGTH = 'max_length[' . 5000 . ']';

        $this->load->library('form_validation');
        $this->form_validation->set_rules('parentID', '', 'required|xss_clean');
        $this->form_validation->set_rules('text', 'Text', 'required|xss_clean|' . $COMMENT_MAX_LENGTH);

        if ($this->form_validation->run()) {
            $this->doSubmit($this->groupmodel->getGroupIDFromName($group), $group);
        }
        echo validation_errors();
    }

    private function doSubmit($groupID, $groupName) {
        $parentSplit = explode("_", $this->input->post('parentID'));
        $parentType = $parentSplit[0];
        $parentID = $parentSplit[1];
        $userID = $this->session->userdata('user_id');

        if ($parentType === 'g') {
            $post = $this->groupmodel->insertPostForGroupID($groupID, $userID, $this->input->post('text'));
            $postHTML = json_encode($this->load->view("templates/post", ['comment' => $post], TRUE));
            ?><script>
                            $("#postsContainer").prepend($(<?= $postHTML ?>).hide().delay(100).slideDown('slow'));
                            $(".submitPostDiv").find("textarea").val("");
            </script><?php
        }
        if ($parentType === 'c') {
            $this->groupmodel->insertCommentForParentID($parentID, $groupID, $userID, $this->input->post('text'));

            $domParentSelector = "'.comment#c_' + $parentID";
            $commentTree = json_encode($this->getCommentTree($groupName, TRUE, $parentID));
            ?><script>
                            addCommentToDOM(<?= $domParentSelector ?>, <?= $commentTree ?>);
            </script><?php
        }
    }

    function getCommentTree($group, $return = FALSE, $postID = NULL) {
        $commentTreeString = "";

        $loopChildren = function($children, $controller) use (&$commentTreeString, &$loopChildren) {
            $loggedIn = $this->session->userdata('logged_in');
            foreach ($children as $child) {
                $commentTreeString .= $controller->load->view('templates/comment', $child['comment'] + ['loggedIn' => $loggedIn], TRUE);
                if ($child['children'] != []) {
                    $loopChildren($child['children'], $controller);
                }
                $commentTreeString .= "</div></div>";
            }
        };

        if (!$postID)
            $postID = $this->input->post('postID');
        if (!$postID)
            dieToHome("Improper post ID.");
        $commentTree = $this->groupmodel->getCommentTree($postID, $group);
        $loopChildren($commentTree['children'], $this);
        if ($return)
            return $commentTreeString;
        echo $commentTreeString;
    }

    function vote() {
        if (!$this->input->post())
            dieToHome("Invalid request.");
        else {
            $voteNum = $this->input->post('voteNum');
            $cID = $this->input->post('commentID');
            $this->groupmodel->setVoteForComment($this->loggedIn, $voteNum, $cID);
        }
    }
    
    function subscribe(){
        $this->groupmodel->setSubscription($this->input->post('type'), $this->groupID);
    }

}
