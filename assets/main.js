function addSpinnerTo(parent, style) {
    style = "style='" + style + "'" || '';
    if (!parent.find(".spinner").length)
        parent.append("<div class='spinner fa fa-circle-o-notch fa-spin fa-lg'" + style + "></div>");
}

function removeSpinner() {
    $(".spinner").fadeOut('slow', function () {
        $(".spinner").remove();
    });
}

$(document).ready(function () {
    $.fn.LoginSubmit = function () {
        addSpinnerTo($("#loginForm"), "margin: auto;");
        $.post($("#loginForm").attr("action"), $("#loginForm :input").serializeArray(),
                function (data) {
                    $("div#loginError").html(data);
                    removeSpinner();
                });
        $("#loginForm").submit(function () {
            return false;
        });
    };
    $.fn.SignUpSubmit = function () {
        addSpinnerTo($("#signUpForm"), "margin: auto;");
        $.post($("#signUpForm").attr("action"), $("#signUpForm :input").serializeArray(),
                function (data) {
                    removeSpinner();
                    $("div#createError").html(data);
                });
        $("#signUpForm").submit(function () {
            return false;
        });
    };

    $(".loginRequired").click(function () {
        alert('do js to redirect to account signin/up page.');
    });

    $('.collaspe').click(function () {
        var content = $(this).children('div').slideToggle();
        $(this).children('i').toggleClass("fa-caret-down");
        $(this).children('i').toggleClass("fa-caret-right");
    });
});

function saveAccount(form) {
    form.parent().find('.error').html('');
    addSpinnerTo(form, 'float:right;');
    $.post("/account/save", form.serializeArray(),
            function (data) {
                removeSpinner();
                form.parent().find('.error').html(data);
            });
}

function setVoteTo(voteNum, votediv) {
    var commentID = votediv.parent().attr('id');
    commentID = commentID.replace('c_', '');
    if (voteNum > -2 && voteNum < 2) {
        $.post(window.location.pathname + "/vote/",
                {commentID: commentID, voteNum: voteNum}, null);
    }
}

function upclick(arrow) {
    var votediv = arrow.parent();
    var voteClass = votediv.attr('class').split(' ')[1];
    if (voteClass === 'upped') {
        votediv.toggleClass(voteClass + " unvoted");
        setVoteTo(0, votediv);
    } else {
        votediv.toggleClass(voteClass + " upped");
        setVoteTo(1, votediv);
    }
}

function downclick(arrow) {
    var votediv = arrow.parent();
    var voteClass = votediv.attr('class').split(' ')[1];
    if (voteClass === 'downed') {
        votediv.toggleClass(voteClass + " unvoted");
        setVoteTo(0, votediv);
    } else {
        votediv.toggleClass(voteClass + " downed");
        setVoteTo(-1, votediv);
    }
}

/* Group Centered Things */

function createGroupPOST(form) {
    addSpinnerTo(form);
    $.post(form.attr("action"), form.serializeArray(),
            function (data) {
                removeSpinner();
                form.find('#error').html(data);
            });
}

function collapseAll(el) {
    var parent = el.parent();
    parent.find(".commentTree").hide(100);
    parent.find(".expander").text("[+]");
}

var closedExpander = "[+]";
var openExpander = "[-]";
function toggleCommentTree(expander) {
    var postID = expander.parent().attr('id').replace('c_', '');
    var tree = expander.parent().find(".commentTree");
    expander.text(expander.text() === closedExpander ? openExpander : closedExpander);
    if (tree.is(":empty")) {
        addSpinnerTo(tree.parent());
        $.post(window.location.pathname + "/getCommentTree", {postID: postID},
        function (data) {
            data = data === "" ? data = "<i>&nbsp;&nbsp;No comments<i>" : data;
            tree.html(data).slideToggle();
            removeSpinner();
        });
    } else
        tree.slideToggle();
}

function showCommentTree(expander) {
    if (expander.text() === closedExpander)
        toggleCommentTree(expander);
}

function addCommentToDOM(domParentSelector, commentTree) {
    var parentComment = $(domParentSelector);
    if (parentComment.parent().hasClass("post")) { // Post level reply
        parentComment.siblings(".commentTree").html(commentTree);
        showCommentTree(parentComment.siblings(".expander"));
    } else {
        parentComment.children('.children').html(commentTree);
    }

    $('.submitReply').hide('fast', function () {
        $('.submitReply').remove();
    });
}

function Submit(form) {
    form.find('button').hide();
    addSpinnerTo(form);
    $.post(window.location.pathname + "/submit", form.serializeArray(),
            function (data) {
                removeSpinner();
                form.find('button').show();
                form.find('#error').html(data);
            });
}

function replyTo(commentID, parent) {
    var textarea = "\
    <div class='submitPostDiv submitReply' style='display: none;'> \
    <form method='POST' accept-charset='utf-8'> \
        <input type='hidden' name='parentID' value=" + commentID + "> \
        <textarea id='replyTextArea' name='text' rows='3' cols='70' placeholder='Reply...'></textarea> \
        <div id='error' class='error'></div> \
        <button type='button' onclick='Submit($(this).parent())'>Post</button> \
        <button type='button' onclick='var box = $(this).parent().parent(); box.hide(200, function(){ box.remove(); });'>Cancel</button> \
    </form></div>";
    if (parent.find("textarea").length === 0) {
        parent.append(textarea);
        parent.find(".submitPostDiv").show(200);
    }
    $(parent).find('#replyTextArea').focus();
}